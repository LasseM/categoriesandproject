const router = require('express').Router()
const controller = require('../controllers/todos.js')


router.post('/create', controller.create)
router.delete('/remove', controller.remove)
router.get('/find_todos', controller.findTodos)
router.get('/find_one_todo/:_id', controller.findOneTodo)
router.put('/update_todo', controller.updateTodo)


module.exports = router