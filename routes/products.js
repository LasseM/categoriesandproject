const router = require('express').Router()
const controller = require('../controllers/products.js')


router.post('/add', controller.add)
router.delete('/remove', controller.remove)
router.get('/find_products', controller.findProducts)
router.get('/find_one_product/:_id', controller.findOneProduct)
router.put('/update_product', controller.updateProduct)


module.exports = router