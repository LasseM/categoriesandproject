const router = require('express').Router()
const controller = require('../controllers/categories.js')


router.post('/add', controller.add)
// router.delete('/remove', controller.remove)
// router.get('/find_categories', controller.findcategories)
// router.get('/find_one_categories/:_id', controller.findOnecategories)
// router.put('/update_categories', controller.updatecategories)


module.exports = router