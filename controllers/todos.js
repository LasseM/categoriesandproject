const Todos = require('../models/todos.js')
//=====================================================
const create = async (req,res) => {
    console.log(req.body.todo)
    try{

    const response = await Todos.create({todo:req.body.todo})
    console.log(response)
    res.send({ok: true, message: `${response.todo} created in todos collection`})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
//=====================================================

const remove = async (req, res) => {
    try {
        const { _id } = req.body
        const response = await Todos.deleteOne({_id: _id})
        console.log(response)
        res.send({ok: true, message:`Todo removed`})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}

//=====================================================

const findTodos = async (req, res) => {
    try {
        const response = await Todos.find({completed:false})
        
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}

//=====================================================

const findOneTodo = async (req, res) => {
    const { _id } = req.params
    try {
        const response = await Todos.findOne({_id: _id})
        
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
//=====================================================

const updateTodo = async (req, res) => {
   const { _id, todo, completed } = req.body
    try {
        const response = await Todos.updateOne({_id:_id}, {completed: completed})
        console.log(response)      
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}


module.exports = {
    create,
    remove,
    findTodos,
    findOneTodo,
    updateTodo

}