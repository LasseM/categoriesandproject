const Products = require('../models/products.js')


//=====================================================
const add = async (req,res) => {
    console.log(req.body.products)
    try{

    const response = await Products.create({productname:req.body.productname, categoryID:req.body.categoryID, price:req.body.price, color:req.body.color, description:req.body.description})
    console.log(response)
    res.send({ok: true, message: `${response.productname} created in products collection`})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
//=====================================================

const remove = async (req, res) => {
    try {
        const { _id } = req.body
        const response = await Products.deleteOne({_id: _id})
        console.log(response)
        res.send({ok: true, message:`Product removed`})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}

// //=====================================================

const findProducts = async (req, res) => {
    try {
        const response = await Products.find({})
        
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}

//=====================================================

const findOneProduct = async (req, res) => {
    const { _id } = req.params
    try {
        const response = await Products.findOne({_id: _id})
        
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
// //=====================================================

const updateProduct = async (req, res) => {
    console.log(req.body)
   
    const { _id, productname, categoryID, price, color, description } = req.body
   
    try {
        const response = await Products.updateOne({_id:_id}, {productname:productname, categoryID:categoryID, price:price, color:color, description:description})
              
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
     
    }
}




module.exports = {
    add,
    remove,
    findProducts,
    findOneProduct,
    updateProduct
}