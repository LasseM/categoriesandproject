const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const todosSchema = new Schema({
    todo:{
        type: String,
        unique: true,
        require: true
    },
    completed:  {
        type: Boolean,
        default: false
    }

})

module.exports =  mongoose.model('todos', todosSchema);