const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    // todosRoute = require('./routes/TodosRoute'),
    bodyParser = require('body-parser');
// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb://127.0.0.1/categories_and_products', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
app.use('/todos', require('./routes/todos.js'));
app.use('/products', require('./routes/products.js'));
app.use('/categories', require('./routes/categories.js'));
// Set the server to listen on port 3000
app.listen(3000, () => console.log(`listening on port 3000`))